package perimeterx.com;
import perimeterx.com.impl.CountWords;

import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.transforms.*;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TypeDescriptors;
import org.junit.Rule;
import java.util.Arrays;
import java.util.List;

public class DataFlowTest {
    @Rule public final transient TestPipeline p = TestPipeline.create();

    //Our static input data, which will comprise the initial PCollection.
    String[] WORDS_ARRAY = new String[]{
            "hi there", "hi", "hi sue bob",
            "hi sue", "", "bob hi"};

    List<String> WORDS = Arrays.asList(WORDS_ARRAY);


    Iterable<KV<String, Long>>  COUNTS_ARRAY = Arrays.asList(
            KV.of("hi", 5L),
            KV.of("there", 1L),
            KV.of("sue", 2L),
            KV.of("bob", 2L));
    @org.junit.Test
    public void test1() {
        // Create a PCollection from the WORDS static input data.
        PCollection<String> input = p.apply(Create.of(WORDS)).setCoder(StringUtf8Coder.of());

        // Run ALL the pipeline's transforms (in this case, the CountWords composite transform).
        PCollection<KV<String, Long>> output = input.apply(new CountWords());

        output.apply("FormatResults", MapElements
                .into(TypeDescriptors.strings())
                .via((KV<String, Long> wordCount) -> wordCount.getKey() + ": " + wordCount.getValue()))
                .apply(TextIO.write().withoutSharding().to("./temp/test.txt"));

        // Assert that the output PCollection matches the COUNTS_ARRAY known static output data.
        PAssert.that(output).containsInAnyOrder(COUNTS_ARRAY);

        // Run the pipeline.
        p.run();
    }

}
