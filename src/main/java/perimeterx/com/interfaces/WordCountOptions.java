package perimeterx.com.interfaces;

import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;

public interface WordCountOptions extends PipelineOptions {
    @Description("Path of the file to read from")
    @Default.String("./input/kinglear.txt")
    String getInputFile();

    void setInputFile(String value);

    @Description("Pipeline Output path, output file destination")
    @Default.String("./output/output.txt")
    String getOutput();

    void setOutput(String value);
}
