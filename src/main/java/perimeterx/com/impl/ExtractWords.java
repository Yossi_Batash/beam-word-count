package perimeterx.com.impl;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Distribution;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.transforms.DoFn;

class ExtractWords extends DoFn<String, String> {
    private final Counter emptyLines = Metrics.counter(ExtractWords.class, "emptyLines");
    private final Distribution lineLenDist =
            Metrics.distribution(ExtractWords.class, "lineLenDistro");

    @ProcessElement
    public void processElement(@Element String element, OutputReceiver<String> receiver) {
        lineLenDist.update(element.length());
        if (element.trim().isEmpty()) {
            emptyLines.inc();
        }

        // Split the line into words.
        String[] words = element.replaceAll("[!?,-.]", "").split("\\s+", -1);

        // Output each word encountered into the output PCollection.
        for (String word : words) {
            if (!word.isEmpty()) {
                receiver.output(word);
            }
        }
    }
}
