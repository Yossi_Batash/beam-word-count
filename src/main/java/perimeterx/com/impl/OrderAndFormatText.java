package perimeterx.com.impl;

import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.values.KV;
import java.util.ArrayList;
import java.util.function.Supplier;
import java.util.stream.StreamSupport;

public class OrderAndFormatText extends SimpleFunction<KV<String, Iterable<KV<String, Long>>>, String> {
    @Override
    public String apply(KV<String, Iterable<KV<String, Long>>> input) {
        return StreamSupport.stream(input.getValue().spliterator(), false)
                .collect((Supplier<ArrayList<KV<String, Long>>>) ArrayList::new,
                        (al, kv) -> al.add(KV.of(kv.getKey(), kv.getValue())),
                        (sb, kv) -> {
                        })
                .stream()
                .sorted((kv1, kv2) -> kv2.getValue().compareTo(kv1.getValue()))
                .collect(StringBuilder::new,
                        (sb, kv) -> sb.append(String.format("%s : %d%n", kv.getKey(), kv.getValue())),
                        (sb, kv) -> {}).toString();
    }
}