package perimeterx.com.impl;

import org.apache.beam.sdk.transforms.Count;
import org.apache.beam.sdk.transforms.PTransform;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.log4j.Logger;

public class CountWords
        extends PTransform<PCollection<String>, PCollection<KV<String, Long>>> {
    @Override
    public PCollection<KV<String, Long>> expand(PCollection<String> lines) {
        final Logger logger = Logger.getLogger(String.valueOf(CountWords.class));
        logger.info("Extracting words");
        // Convert lines of text into individual words.
        PCollection<String> words = lines.apply(ParDo.of(new ExtractWords()));
        logger.info("Count words");
        // Count the number of times each word occurs.
        PCollection<KV<String, Long>> wordCounts = words.apply(Count.perElement());

        return wordCounts;
    }
}