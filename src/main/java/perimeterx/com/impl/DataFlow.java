package perimeterx.com.impl;
import com.codahale.metrics.MetricRegistry;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.*;
import org.apache.beam.sdk.values.KV;
import perimeterx.com.interfaces.WordCountOptions;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;


public class DataFlow {
    public final static Logger logger = Logger.getLogger(String.valueOf(DataFlow.class));
    static MetricRegistry metricRegistry = new MetricRegistry();
    static void runDataFlow(WordCountOptions options) {
        Pipeline p = Pipeline.create(options);

        // Concepts #2 and #3: Our pipeline applies the composite CountWords transform, and passes the
        // static FormatAsTextFn() to the ParDo transform.
        try {

            p.apply("ReadLines", TextIO.read().from(options.getInputFile()))
                    .apply("CountWords", new CountWords())

                    .apply("CreateKey", ParDo.of(new DoFn<KV<String, Long>, KV<String, KV<String, Long>>>() {
                        @ProcessElement
                        public void processElement(ProcessContext c) {
                            KV<String, Long> element = c.element();
                            String key = element.getKey();
                            c.output(KV.of("single", KV.of(key, element.getValue())));
                        }
                    }))
                    .apply("GroupByKey", GroupByKey.create())
                    .apply("OrderAndFormatText", MapElements.via(new OrderAndFormatText()))
                    .apply("WriteCounts", TextIO.write().withoutSharding().to(options.getOutput()));

            p.run().waitUntilFinish();
            metricRegistry.counter("Success").inc();
        }catch(Exception e){
            logger.error("Fail Running DataFlow Error: ", e);
            metricRegistry.counter("Failure").inc();
        }
    }

    public static void main(String[] args) {
        BasicConfigurator.configure();
        logger.info("Start running beam word counter");

        WordCountOptions options = PipelineOptionsFactory.fromArgs(args).withValidation()
                .as(WordCountOptions.class);

        runDataFlow(options);
        logger.info("Shutting down application");
    }
}