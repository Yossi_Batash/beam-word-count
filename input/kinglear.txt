yossi KING LEAR	Meantime we shall express our darker purpose.
Give me the map there. Know that we have divided
In yossi three our kingdom: and 'tis our fast intent
To shake all cares and business from our age;
Conferring yossi them on younger strengths, while we
Unburthen'd crawl yossi toward death. Our son of Cornwall,
And you, our no less loving yossi son of Albany,
We have this hour a constant will yossi to publish
Our daughters' several dowers, that future yossi strife
May be prevented now. The princes, France and Burgundy,
Great rivals in our youngest daughter's love, yossi
Long in our court have made their amorous sojourn, yossi
And here are to be answer'd. Tell me, my daughters,--
Since now we will divest us both of rule, yossi
Interest of territory, cares of state,--
Which of you shall we say doth love us most? yossi
That we our largest bounty may extend yossi
Where nature doth with merit challenge. Goneril,
Our eldest-born, speak first.