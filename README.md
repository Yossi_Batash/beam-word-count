beam-word-count
--=============

What Is This?
This program intended to provide a framework that execute the next data flow:
1. read text from an input data file
2. transform lines into words
3. count word instances
4. sum the count of each word to get key/value pairs of words and their counts
5. order by count descending
6. format each pair into a printable string
7. write text to an output file 

How To Install This Program:
1.Clone this repository to the server/machine in which you want to execute this program.
2.Set a SDK to the project
3.Chose the pom.xml file under Maven configurations
4.Run install dependencies via maven (make sure that the test successfully)
5.Execute the WordCount.py class
If you have any questions fell free to ask
Enjoy :)